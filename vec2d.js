if (!jsludge.ext) jsludge.ext = {};
jsludge.ext.vec2d = (function () {
    var Vector2D, c = jsludge.classes;

    Vector2D = Object.extend(function (x, y) {
        this.set(x || 0, y || 0);
    }, {
        'set': function (x, y) {
            if (x === this.x && y === this.y) return this;
            this.x = x;
            this.y = y;
            // invalidate cached data
            this.angle = null;
            this.magnitude = null;
            return this;
        },
        'getAngle': function () {
            if (!this.angle) this.angle = Math.atan2(this.y, this.x);
            return this.angle;
        },
        'getMagnitude': function () {
            if (!this.magnitude) this.magnitude = Math.sqrt(this.x * this.x + this.y * this.y);
            return this.magnitude;
        },
        'getRightNormal': function () {
            if (!this.rightNormal) this.rightNormal = new Vector2D();
            this.rightNormal.set(-this.y, this.x);
            return this.rightNormal;
        },
        'getLeftNormal': function () {
            if (!this.leftNormal) this.leftNormal = new Vector2D();
            this.leftNormal.set(this.y, -this.x);
            return this.leftNormal;
        },
        'getUnitVector': function () {
            if (!this.unitVector) this.unitVector = new Vector2D();
            this.unitVector.set(this.x / this.getMagnitude(), this.y / this.getMagnitude());
            return this.unitVector;
        },
        'rotate': function (angle) {
            return this.set(this.x * Math.cos(angle) - this.y * Math.sin(angle),
                this.x * Math.sin(angle) + this.y * Math.cos(angle));
        },
        'rotate90': function () {
            return this.set(-this.y, this.x);
        },
        'rotateNeg90': function () {
            return this.set(this.y, -this.x);
        },
        'add': function (b) {
            return new Vector2D(this.x + b.x, this.y + b.y);
        },
        'sub': function (b) {
            return new Vector2D(this.x - b.x, this.y - b.y);
        },
        'translate': function (x, y) {
            return new Vector2D(this.x + x, this.y + y);
        },
        'dotProduct': function (b) {
            return (this.x * b.x) + (this.y * b.y);
        },
        'projectOn': function (b) {
            return this.dotProduct(b.getUnitVector());
        },
        'scalarMul': function (v) {
            return new Vector2D(this.x * v, this.y * v);
        },
        'scalarAdd': function (v) {
            return new Vector2D(this.x + v, this.y + v);
        },
        'draw': function (ctx, xoff, yoff) {
            xoff = xoff || 0;
            yoff = yoff || 0;
            ctx.strokeStyle = '#ff0000';
            ctx.lineWidth = 1;
            ctx.beginPath();
            ctx.moveTo(xoff, yoff);
            ctx.lineTo(xoff + this.x, yoff + this.y);
            ctx.stroke();
            ctx.closePath();
        }
    });

    return {
        'Vector2D': Vector2D
    }
}());
