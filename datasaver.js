/**
 * Allows saving/loading data to a user's machine, either
 * via localstorage or coookies.
 * TODO: later on, support saving with AJAX and stuff,
 * with another class w/the same interface
 */
if (!jsludge.ext) jsludge.ext = {};
jsludge.ext.datasaver = (function () {
    var DataSaver;

    DataSaver = Object.extend(function (gameUID) {
        this.uid = gameUID;

        // see if user's browser supports local storage or coookies

        // load data if it exists
    }, {
        'save': function () {
        },
        'load': function () {
        }
    });

    return {
        'DataSaver': DataSaver
    }
}());
