var SnakeGame;
(function (SnakeGame) {
  SnakeGame.gfxdata =  {
    images: {
      sprites: 'snake.png',
      bg: 'bgtiles.png'
    },
    player:{img:'sprites',x:0,y:0,w:32,h:32,rx:0,ry:0,anim:{idle:[1,2],hit:[1]}}
  };

  var Player = (function(){
    function Player(x,y) {
      jsludge.AnimatedSprite.call(this,x,y,SnakeGame.gfxdata.player);
    }
    Player.prototype = Object.create(jsludge.AnimatedSprite.prototype);
    
    return Player;
  }());
  jsludge.Player = Player;

  var PlayState = (function(){
    function PlayState() {
      jsludge.State.call(this);
      this.sprites.push(new Player(100,100));
    }
    PlayState.prototype = Object.create(jsludge.State.prototype);

    return PlayState;
  }());
  SnakeGame.PlayState = PlayState;

  SnakeGame.newGame = function() {
    jsludge.switchState(new PlayState);
  }

  var MenuState = (function(){
    function MenuState() {
      jsludge.State.call(this);
      this.addButton(100,100,SnakeGame.newGame,"hi",30,30);
    }
    MenuState.prototype = Object.create(jsludge.State.prototype);
    
    return MenuState;
  }());
  SnakeGame.MenuState = MenuState;

  SnakeGame.init = function() {
    jsludge.util.addListener(window, 'load', function() {
      jsludge.init(new MenuState, new jsludge.CanvasRenderer(700,500,1,'#eee'), SnakeGame.gfxdata, 2);
    });
  };
})(SnakeGame || (SnakeGame = {}));

SnakeGame.init();

