var jsludge;
(function (jsludge) {
  var CanvasRenderer = (function(){
    function CanvasRenderer(w,h,layers,bgcolor) {
      this.numLayers = layers;
      this.layers = [];
      jsludge.Renderer.call(this,w,h,bgcolor);
      this.surface = this.makeSurface(w,h,bgcolor || 'transparent');
      this.w = this.surface.canvas.width;
      this.h = this.surface.canvas.height;
    }
    CanvasRenderer.prototype = Object.create(jsludge.Renderer.prototype);

    CanvasRenderer.prototype.makeSurface = function(w,h,bgcolor) {
      var div = document.createElement('div'), e, i;
      div.style.backgroundColor = bgcolor;
      div.style.width = w+'px';
      div.style.height = h+'px';
      div.setAttribute('id','game');

      for (i = 0; i < this.numLayers; i++) {
        e = document.createElement('canvas');
        e.setAttribute('id','layer'+i);
        e.style.position = 'absolute';
        e.style.top = '0';
        e.style.left = '0';
        e.setAttribute('width',w+'px');
        e.setAttribute('height',h+'px');
        e.style.backgroundColor = 'transparent';
        div.appendChild(e);
        i++;
      }

      document.body.appendChild(div);

      this.offX = e.offsetLeft;
      this.offY = e.offsetTop;

      return e.getContext('2d');
    };

    CanvasRenderer.prototype.drawSprite = function(id, layer, img, src, dst) {
      this.surface.drawImage(img,
        src.x, src.y, src.w, src.h,
        dst.x, dst.y, dst.w, dst.h);
    };

    CanvasRenderer.prototype.drawTiles = function(mapdata, view, gfxdata, img) {
      // ref: http://www.gamedev.net/page/resources/_/technical/game-programming/smooth-scrolling-a-tile-map-r743
      var i, lasttile, vx, vy, xoff, yoff, tile, spr, dx, dy;

      // the x and y location of the view, in tiles
      vx = (view.x/gfxdata.tw) >> 0;
      vy = (view.y/gfxdata.th) >> 0;

      // sub-tile offset, for smooth map scrolling
      xoff = -(view.x >> 0) % gfxdata.tw;
      yoff = -(view.y >> 0) % gfxdata.th;

      var mw = mapdata.w,
          mh = mapdata.h;
      var tiles = (view.tilesx * view.tilesy);
      var alltiles = mapdata.w * mapdata.h;

      for (i = 0; i < tiles; i++) {
        dx = i % view.tilesx;
        dy = (i / view.tilesx) >> 0;
        tile = (vy * mw) + (dy * mw) + vx + dx;

        // FIXME: get rid of this check
        // why is it trying to draw the n+1th tile???
        if (tile < alltiles) {
          spr = gfxdata.tiles[mapdata[tile]];
          this.surface.drawImage(img,
            spr.x, spr.y, gfxdata.tw, gfxdata.th,
            xoff + (dx * gfxdata.tw),
            yoff + (dy * gfxdata.th),
          gfxdata.tw, gfxdata.th);
        }
      }
    };

    CanvasRenderer.prototype.clear = function() {
      this.surface.clearRect(0,0,this.w,this.h);
    };

    CanvasRenderer.prototype.fillRect = function(color,x,y,w,h) {
      this.surface.fillStyle = color;
      this.surface.fillRect(x,y,w,h);
      //this.surface.drawText(ctx, x + 15, y + 30, text, 'black');
    };

    CanvasRenderer.prototype.drawText = function (x, y, text, color, size, font) {
      if (color) this.surface.fillStyle = color;
      if (font == undefined) font = 'Arial';
      if (size == undefined) size = '30px';
      this.surface.font = size + " " + font;
      this.surface.fillText(text, x, y);
    };

    return CanvasRenderer;
  }());
  jsludge.CanvasRenderer = CanvasRenderer;
})(jsludge || (jsludge = {}));

