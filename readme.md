# JSLUDGE
JSLUGE is a conceptual art piece exploring the constant change of web
technology. Both current and obsolete technologies are used, forming a
"sludge" of the old and new.

Oh, it's also an easy to use games and graphics library.
