/**
 * Super simple circle-circle physics for prototyping.
 */
if (!jsludge.ext) jsludge.ext = {};
jsludge.ext.simplephys = (function () {
    var Circle, tickCircles,
        g = jsludge.g,
        Vector2D = jsludge.ext.vec2d.Vector2D,
        FULL_CIRC = Math.PI * 2;

    /**
     * a 2D circle
     * @param x the center point's x
     * @param y the center point's y
     * @param r the radius of the circle
     */
    Circle = Object.extend(function (x, y, r, boundingBox) {
        this.x = x || 0;
        this.y = y || 0;
        this.r = r || 1;
        this.dx = 0;
        this.dy = 0;
        this.maxdx = 1.5;
        this.maxdy = 1.5;
        this.boundingBox = boundingBox || {x:0,y:0,w:0,h:0};
    }, {
        tick: function (dt) {
            var w = this.boundingBox.w, h = this.boundingBox.h, d = this.r * 2;

            this.x += this.dx * dt;
            this.y += this.dy * dt;

            if ((this.x + d) > w) {
                this.x = w - d - 2;
                this.dx = -this.dx;
            } else if ((this.x - d) < this.boundingBox.x) {
                this.x = this.boundingBox.x + d + 2;
                this.dx = -this.dx;
            }

            if ((this.y + d) > h) {
                this.y = h - d - 2;
                this.dy = -this.dy;
            } else if ((this.y - d) < this.boundingBox.y) {
                this.y = this.boundingBox.y + d + 2;
                this.dy = -this.dy;
            }

            if (this.dx > this.maxdx) this.dx = this.maxdx;
            else if (this.dx < -this.maxdx) this.dx = -this.maxdx;
            if (this.dy > this.maxdy) this.dy = this.maxdy;
            else if (this.dy < -this.maxdy) this.dy = -this.maxdy;
        },

        /**
         * Get distance from our center to center of circle b
         */
        dist: function (b) {
            var a = this,
                dx = b.x - a.x,
                dy = b.y - a.y;
            return Math.sqrt((dx * dx) + (dy * dy));
        },

        /**
         * Does this circle overlap circle B? If so,
         */
        overlapCirc: function (b) {
            var a = this,
                dx = b.x - a.x,
                dy = b.y - a.y,
                // sum of radii
                sr = a.r + b.r;
            return ((dx * dx) + (dy * dy)) < (sr * sr);
        },

        /**
         * Expects both parameters to have x and y properties
         * @param a the start point of the line segment
         * @param b the end point of the line segment
         */
        overlapLine: function (a, b) {
            // v - vector from a to this circle
            var v = new Vector2D(this.x - a.x, this.y - a.y),
                line = new Vector2D(b.x - a.x, b.y - a.y),
                // rotate v -90 degrees
                leftNormal = line.getLeftNormal(),
                // calcuate the line segement's perpendicular distance to this circle
                c1OnNormal = v.projectOn(leftNormal),
                c1OnLine = v.projectOn(line);
            return Math.abs(c1OnNormal) <= this.r
                && line.dotProduct(v) > 0
                && c1OnLine < line.getMagnitude();
        },

        /**
         * Draws the vectors used to solve overlapLine
         */
        debugOverlapLine: function (ctx, a, b) {
            // v - vector from a to this circle
            var v = new Vector2D(this.x - a.x, this.y - a.y),
                line = new Vector2D(b.x - a.x, b.y - a.y),
                // rotate v -90 degrees
                leftNormal = line.getLeftNormal();

            v.draw(ctx, a.x, a.y);
            leftNormal.draw(ctx, a.x, a.y);
            line.draw(ctx, a.x, a.y);
        },

        /**
         * Does this circle overlap circle B? If so,
         * resolve collision with circle B.
         */
        hitCirc: function (b) {
            var a = this,
                dx = b.x - a.x,
                dy = b.y - a.y,
                //angle, vx, vy, bounce,
                tx, ty, ax, ay,
                dist = Math.sqrt(dx * dx + dy * dy),
                minDist = a.r + b.r;
            if (dist < minDist) {
                //bounce = Math.min(a.bounce, b.bounce);
                tx = a.x + dx / dist * minDist;
                ty = a.y + dy / dist * minDist;
                ax = tx - b.x;
                ay = ty - b.y;
                // separate
                a.x -= ax;
                a.y -= ay;
                b.x += ax;
                b.y += ay;
                // apply changes in velocity
                a.dx -= ax;
                a.dy -= ay;
                b.dx += ax;
                b.dy += ay;
            }
        },

        draw: function (ctx) {
            var x = Math.round(this.x),
                y = Math.round(this.y);
            ctx.beginPath();
            ctx.arc(x, y, this.r, FULL_CIRC, false);
            ctx.fillStyle = 'rgba(255,255,255,0.2)';
            ctx.fill();
            ctx.closePath();

            ctx.beginPath();
            ctx.arc(x, y, 1, FULL_CIRC, false);
            ctx.fillStyle = '#bbb';
            ctx.fill();
            ctx.closePath();
        },

        destroy: function () {
            jsludge.util.destroy(this);
        }
    });

    tickCircles = function (cs) {
        var len = cs.length;
        for (var i = 0; i < len; ++i) {
            for (var j = i + 1; j < len; ++j) {
                cs[i].hitCirc(cs[j]);
            }
        }
    };

    return {
        tickCircles: tickCircles,
        Circle: Circle
    }
}());
