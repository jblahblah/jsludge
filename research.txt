# JSLUDGE BACKWARDS COMPATIBILITY RESEARCH
I have a strange urge to make jsludge backwards compatible, to an obscene
degree. I want to support obsolete browsers that no one has used for five years.

So, here's where I document my research and attempts to solve some of the more
difficult problems with this.

The first priority is going to be IE5.5 and 6 support, then Netscape 4.

Then IE7 and 8.

Also, in the future: a CSS3 renderer and an SVG renderer.

## The plan
- make support grid
  - image drawing
  - spritesheets
- CSSRenderer
  - use mibbu.js for reference
  - write spritesheet split script for NS4 (and IE4?)
- VMLRenderer
  - use excanvas.js for reference
- JavaRenderer
- CSS3Renderer
  - shape drawing isn't too hard with this
  - use VMLRenderer for reference
- SVGRenderer
  - use VMLRenderer for reference
- VRMLRenderer

## Stage 1: Drawing images and rectangles, moving them around
* moving around stuff in a cross-browser way is easy, see js book
* CSS sprites
  * ie6: http://www.julienlecomte.net/blogfiles/ie6-alpha-transparency-sprites/
  * ns4: no CSS sprites, whatsoever. The necessary CSS properties don't work.
    I've tried techniques using overflow, background-position, and clip. They
    all fail. Here are the only attractive alternatives:
    * CSS positioning, but split spritesheets into individual images using a 
      node or python script.
    * talk to a Java applet via Java interop

## Stage 2: Dynamically drawing shapes
For IE4, possibly VBScript.

### IE4
* VBScript can be used to draw vectors and stuff, there's even a loadImage function,
but can it interop with js?
  * http://stackoverflow.com/questions/1039341/calling-vbscript-from-javascript
  * http://stackoverflow.com/a/12145637
  * looks like you just call the function as normal.
* how to even get IE4 running? may have to buy copy of Win95

### IE5
IE5.5 and later support VML. IECanvas might even work with this.

### IE6
### NETSCAPE 4
* Java interop via LiveConnect

## Stage 3: modifying pixels
* IE5.5+ - VML probably supports this?
* Netscape - use LiveConnect
