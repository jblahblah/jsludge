if (typeof Object.create !== 'function') {
  Object.create = function (o) {
    var F = function () {
    };
    F.prototype = o;
    return new F();
  };
}

// define the core of jsludge.
// any extensions should be added as properties to jsludge.ext
var jsludge;
(function (jsludge) {
  // ----------------------------------------------------------------------
  // private properties
  var _state, _tick, _onkeydown, _onkeyup, _onmousedown, _onmouseup,
  _onmousemove, _addlisteners, _removelisteners,_lastID = 0, 

  // ----------------------------------------------------------------------
  // constants
  DEFAULT_FPS = 30;

  // ----------------------------------------------------------------------
  // util
  jsludge.util = {
    extend: function (dest, src) {
      for (var prop in src) {
        if (src.hasOwnProperty(prop)) dest[prop] = src[prop];
      }
      return dest;
    },
    getID: function() {
      return _lastID++;
    },
    addListener: function (elem, e, handler) {
      if (elem.addEventListener) {
        elem.addEventListener(e, handler, false);
      } else if (elem.attachEvent) {
        elem.attachEvent('on' + e, handler);
      }
      return elem;
    },
    removeListener: function (elem, e, handler) {
      if (elem.removeEventListener) {
        elem.removeEventListener(e, handler, false);
      } else if (elem.detachEvent) {
        elem.detachEvent('on' + e, handler);
      }
      return elem;
    },
    destroyArray: function (a) {
      var i = a.length;
      while (i--) {
        a[i].destroy();
        delete a[i];
      }
    },
    destroy: function (o) {
      var prop;
      for (prop in o) {
        if (o.hasOwnProperty(prop)) {
          delete o[prop];
        }
      }
      return o;
    },

    pointInRect: function (x, y, w, h, x1, y1) {
      return ((x1 >= x && x1 <= (x + w)) &&
      (y1 >= y && y1 <= (y + h)));
    },

    hitboxCollide: function (r1, r2) {
      return !(r2.left > r1.right ||
        r2.right < r1.left ||
        r2.top > r1.bottom ||
      r2.bottom < r1.top);
    },

    clamp: function(n,low,high) {
      if (n < low) n = low;
      else if (n > high) n = high;
      return n;
    },

    addStats: function () {
      if (Stats && !this.stats) {
        var stats = this.stats = new Stats();

        // Align top-left
        stats.getDomElement().style.position = 'absolute';
        stats.getDomElement().style.right = '0px';
        stats.getDomElement().style.top = '0px';

        setInterval(function () {
          stats.update();
        }, 1000 / 60);
        document.body.appendChild(stats.getDomElement());
      }
    }
  };

  // ----------------------------------------------------------------------
  // globals
  jsludge.g = {
    keysdown: {},
    gfxdata: {},
    maps: {},
    mouseX: 0,
    mouseY: 0,
    lmb: false,
    rmb: false,
    frame: 0
  };

  // ----------------------------------------------------------------------
  // Renderer
  var Renderer = (function(){
    function Renderer(w,h,bgcolor) {
      this.w = w;
      this.h = h;
      this.bgcolor = bgcolor;
      this.offX = 0;
      this.offY = 0;
    }

    /**
    * @desc draw a sprite on the screen
    * @method drawSprite
    * @param {} img
    * @param {jsludge.Rect} src
    * @param {jsludge.Rect} dst
    */
    Renderer.prototype.drawSprite = function(id, layer, img, src, dst) {
    };

    /**
    * @desc draws the visible portion of the map
    * @method drawTiles
    * @param {Array} mapdata
    * @param {jsludge.Rect} view
    * @param {Object} gfxdata
    */
    Renderer.prototype.drawTiles = function(mapdata, view, gfxdata) {};

    // override these in each renderer to provide a basic canvas API
    Renderer.prototype.clear = function() {};
    Renderer.prototype.clearAllLayers = function() {};
    Renderer.prototype.clearRect = function(x,y,w,h) {};
    Renderer.prototype.onResize = function(w,h) {};
    Renderer.prototype.fillRect = function(x,y,w,h,color) {};
    Renderer.prototype.drawText = function(text, color, size, font){};

    Renderer.prototype.getXOffset = function() {
      return this.offY;
    };

    Renderer.prototype.getYOffset = function() {
      return this.offX;
    };

    Renderer.prototype.loadImages = function(images) {
      var id, src, div, e;

      // make invisible container div
      div = document.createElement('div');
      div.setAttribute('id','assets');
      div.style.visibility = 'hidden';

      // add all images to it
      for (id in images) {
        src = images[id];
        e = document.createElement('img');
        e.setAttribute('src',src);
        e.setAttribute('id',id);
        div.appendChild(e);
      }

      document.body.appendChild(div);
    };

    Renderer.prototype.getImage = function (id) {
      return document.getElementById(id);
    };

    Renderer.prototype.destroy = function() {
      this.clear();
      jsludge.util.destroy(this);
    };

    // shapes api
    // (need to think of general API for this, copy PHIGS?)
    Renderer.prototype.moveTo = function () { };
    Renderer.prototype.rect = function () { };
    Renderer.prototype.circle = function () { };
    Renderer.prototype.oval = function () { };
    Renderer.prototype.arc = function () { };
    Renderer.prototype.lineTo = function () { };
    Renderer.prototype.stroke = function () { };
    Renderer.prototype.fill = function () { };

    // pixel api
    Renderer.prototype.getpixel = function (x, y, r, g, b, a) { };
    Renderer.prototype.putpixel = function (x, y) { };

    return Renderer;
  }());
  jsludge.Renderer = Renderer;

  var Map = (function(){
    function Map() {
      this.data = null;
      this.view = new Rect();
      this.gfxdata = null;
      this.img = null;
    }

    /** gets the map ready for drawing */
    Map.prototype.init = function() {
      this.img = jsludge.g.renderer.getImage(this.gfxdata.img);
      this.view.w = jsludge.g.renderer.w;
      this.view.h = jsludge.g.renderer.h;
      this.view.tilesx = Math.ceil(this.view.w / this.gfxdata.tw) + 1;
      this.view.tilesy = Math.ceil(this.view.h / this.gfxdata.th) + 1;
    };

    Map.prototype.draw = function() {
      // HACK
      if (!this.img) this.init();
      jsludge.g.renderer.drawTiles(this.data, this.view, this.gfxdata, this.img);
    };

    Map.prototype.scroll = function(dx,dy) {
      this.view.x = jsludge.util.clamp(this.view.x + dx, 0, this.data.imgw - this.view.w);
      this.view.y = jsludge.util.clamp(this.view.y + dy, 0, this.data.imgh - this.view.h);
    };

    Map.prototype.destroy = function() {
      jsludge.util.destroy(this);
    };
    
    return Map;
  }());
  jsludge.Map = Map;

  /** loads maps created with Tiled and exported as JSON */
  jsludge.mapLoaderTiledJSON = function(id,data,imgid) {
    var map = new jsludge.Map();
    map.data = data.layers[0].data;
    map.data.w = data.width;
    map.data.h = data.height;
    map.data.imgw = data.tilesets[0].tilewidth * data.width;
    map.data.imgh = data.tilesets[0].tileheight * data.height;
    map.gfxdata = {
      img: data.tilesets[0].image,
      tw: data.tilesets[0].tilewidth,
      th: data.tilesets[0].tileheight,
      w: data.tilesets[0].imagewidth,
      h: data.tilesets[0].imageheight
    };
    // calc tiles
    var tiles = [],
        tw = data.tilesets[0].tilewidth,
        th = data.tilesets[0].tileheight,
        lenx = data.tilesets[0].imagewidth / tw, 
        leny = data.tilesets[0].imageheight / th, 
        i,
        len = lenx*leny;

    for (i = 0; i < len; i++) {
      tiles[i] = new jsludge.Rect(
        (i % lenx) * tw,
        ((i / lenx) >> 0) * th,
        tw, th);
    }

    // gross, tiled is 1-indexed, so we have to convert it to 0-indexes
    i = map.data.length;
    while (i--) map.data[i] -= 1;

    map.gfxdata.tiles = tiles;
    map.gfxdata.img = imgid;
    map.view.w = lenx;
    map.view.h = leny;
    jsludge.g.maps[id] = map;
    return map;
  };

  /**
   * Use this to create a Map object from data. Reassign this fn to use
   * different map loaders. By default it loads a JSON file as exported by
   * the map editor Tiled.
   */
  jsludge.loadMap = jsludge.mapLoaderTiledJSON;

  var Rect = (function(){
    function Rect(x,y,w,h) {
      this.x = x || 0;
      this.y = y || 0;
      this.w = w || 0;
      this.h = h || 0;
    }

    Rect.prototype.set = function(x,y,w,h) {
      this.x = x;
      this.y = y;
      this.w = w;
      this.h = h;
    };

    return Rect;
  }());
  jsludge.Rect = Rect;

  // ----------------------------------------------------------------------
  // State
  var State = (function(){
    function State(x, y) {
      this.buttons = new SpriteList;
      this.sprites = new SpriteList;
      this.x = x || 100;
      this.y = y || 100;
    }

    State.prototype.addButton = function (x, y, callback, text, w, h) {
      this.buttons.push(new Button(x, y, callback, text, w, h));
    };

    State.prototype.draw = function () {
      //var ctx =jsludge.g.ctxs[0];
      this.sprites.draw(jsludge.g.renderer);
      this.buttons.draw(jsludge.g.renderer);
    };

    State.prototype.tickSprites = function () {
      this.sprites.tick();
      this.buttons.tick();
    };

    State.prototype.tick = function () {
      this.tickSprites();
      this.draw();
    };

    State.prototype.onkeyup = function (e) {
    };

    State.prototype.onkeydown = function (e) {
    };

    State.prototype.onmouseup = function (e) {
      var offX = jsludge.g.renderer.getXOffset(),
          offY = jsludge.g.renderer.getYOffset(),
          s,
          i;
      for (i = 0; this.buttons && i < this.buttons.length; i++) {
        s = this.buttons[i];
        if (jsludge.util.pointInRect(s.x, s.y, s.w, s.h,
          e.clientX - offX,
        e.clientY - offY)) {
          s.click();
        } else {
          s.onmouseup();
        }
      }
    };

    State.prototype.onmousedown = function (e) {
      var offX = jsludge.g.renderer.getXOffset(),
          offY = jsludge.g.renderer.getYOffset(),
          s,
          i;
      for (i = 0; i < this.buttons.length; i++) {
        s = this.buttons[i];
        if (jsludge.util.pointInRect(s.x, s.y, s.w, s.h,
          e.clientX - offX,
        e.clientY - offY)) {
          s.onmousedown();
        }
      }
    };

    State.prototype.onmousemove = function (e) {
      var i;
      for (i = 0; i < this.buttons.length; i++) {
        this.buttons[i].onmousemove(e);
      }
    };

    State.prototype.destroy = function () {
      jsludge.g.renderer.clear();
      jsludge.util.destroy(this.sprites);
      jsludge.util.destroy(this.buttons);
      jsludge.util.destroy(this);
    };

    /**
    * Clear all canvases
    */
    State.prototype.clearAllCanvases = function () {
     jsludge.g.renderer.clearAllLayers();
    };

    /**
    * Clear the canvas at index i
    */
    State.prototype.clearCanvas = function (i) {
     jsludge.g.renderer.clear();
    };
    return State;
  }());
  jsludge.State = State;

  // ----------------------------------------------------------------------
  // Sprite
  var Sprite = (function(){
    function Sprite(x, y) {
      this.x = x || 0;
      this.y = y || 0;
      this.dx = 0;
      this.dy = 0;
      this.alive = true;
      this.solid = true;
      this.layer = 0;
      this.id = jsludge.util.getID();
    }

    Sprite.prototype.tick = function () {
      this.x += this.dx;
      this.y += this.dy;
    };

    Sprite.prototype.at = function (x, y) {
      return this.x == x && this.y == y;
    };

    Sprite.prototype.draw = function (canvas) {
    };

    Sprite.prototype.setLayer = function (layer) {
      this.layer = layer;
    };

    Sprite.prototype.destroy = function () {
      jsludge.util.destroy(this);
    };

    return Sprite;
  }());
  jsludge.Sprite = Sprite;

  // ----------------------------------------------------------------------
  // Animated Sprite
  var AnimatedSprite = (function(){
    function AnimatedSprite(x,y,gfxdata) {
      Sprite.call(this,x,y);
      this.gfxdata = gfxdata;
      this.frame = 0;
      this.animating = true;
      this.setAnim('idle');
    }
    AnimatedSprite.prototype = Object.create(Sprite.prototype);

    AnimatedSprite.prototype.setAnim = function (animName) {
      this.anim = animName;
      this.animdata = this.gfxdata.anim[this.anim];
      this.src = new jsludge.Rect;
      this.dst = new jsludge.Rect;
      this.img = jsludge.g.renderer.getImage(this.gfxdata.img);
    };

    AnimatedSprite.prototype.tick = function () {
      Sprite.prototype.tick.apply(this);
      if (this.animating) this.frame++;
    };

    // get the width of this sprite
    AnimatedSprite.prototype.w = function () {
      return this.gfxdata.w;
    };

    // get the height of this sprite
    AnimatedSprite.prototype.h = function () {
      return this.gfxdata.h;
    };

    AnimatedSprite.prototype.draw = function (renderer) {
      var d = this.gfxdata,
      a = this.animdata,
      f = a[this.frame % a.length],
      w = d.w,
      h = d.h;

      this.src.set(d.x + (f * w), d.y, w, h);
      this.dst.set(this.x, this.y, w, h);

      renderer.drawSprite(this.id, this.layer, this.img, this.src, this.dst);
    };

    return AnimatedSprite;
  }());
  jsludge.AnimatedSprite = AnimatedSprite;

  // ----------------------------------------------------------------------
  // SpriteList
  var SpriteList = (function(){
    function SpriteList() {
      Array.call(this);
    }
    SpriteList.prototype = Object.create(Array.prototype);

    SpriteList.prototype.tick = function () {
      for (var i = 0, len = this.length; i < len; i++) {
        this[i].tick();
      }
    };

    SpriteList.prototype.draw = function (renderer) {
      for (var i = 0, len = this.length; i < len; i++) {
        this[i].draw(renderer);
      }
    };

    /**
    * removes any dead sprites
    */
    SpriteList.prototype.prune = function () {
      var i = this.length;
      while (i--) {
        if (!this[i] || !this[i].alive) {
          this[i].destroy();
          this.splice(i, 1);
        }
      }
    };

    SpriteList.prototype.destroy = function () {
      for (var i = 0, len = this.length; i < len; i++) {
        jsludge.util.destroy(this[i]);
      }
      jsludge.util.destroy(this);
    }
    return SpriteList;
  }());
  jsludge.SpriteList = SpriteList;

  // ----------------------------------------------------------------------
  // Button
  var Button = (function(){
    function Button(x, y, action, text, w, h) {
      Sprite.call(this,x,y);
      this.anim = 0;
      this.x = x || 0;
      this.y = y || 0;
      this.w = w || 100;
      this.h = h || 50;
      this.action = action;
      this.text = text || '';
      this.mouseover = false;
      this.mousedown = false;
      this.activated = false;
    }
    Button.prototype = Object.create(Sprite.prototype);

    Button.prototype.click = function () {
      if (!this.activated) {
        this.mousedown = false;
        this.activated = true;
        this.action();
      }
    };

    Button.prototype.onmouseup = function (e) {
      this.mousedown = false;
    };

    Button.prototype.onmousedown = function (e) {
      this.mousedown = true;
    };

    Button.prototype.onmousemove = function (e) {
      if (jsludge.util.pointInRect(this.x, this.y, this.w, this.h, jsludge.g.mouseX,jsludge.g.mouseY)) {
        this.onmouseover();
      } else {
        this.onmouseleave();
      }
    };

    Button.prototype.onmouseover = function () {
      this.mouseover = true;
    };

    Button.prototype.onmouseleave = function () {
      this.mouseover = false;
    };

    Button.prototype.tick = function () {
    };

    Button.prototype.draw = function (renderer) {
      if (this.mousedown) {
        this.color = '#eeee00';
      } else if (this.mouseover) {
        this.color = '#ff33ee';
      } else if (this.mousedown) {
        this.color = '#ff33ee';
      } else {
        this.color = '#ff0000';
      }
      renderer.fillRect(this.color, this.x, this.y, this.w, this.h);
      renderer.drawText(this.x + 15, this.y + 30, this.text, 'black');
    };

    Button.prototype.destroy = function () {
      this.action = null;
      Sprite.prototype.destroy.call(this);
    };
    return Button;
  }());
  jsludge.Button = Button;

  // ----------------------------------------------------------------------
  // top-level listeners
  _tick = function (e) {
    _state.tick(e);
   jsludge.g.frame++;
  };

  _onkeydown = function (e) {
    if (jsludge.g.keysdown.hasOwnProperty[e.keyCode] 
    && jsludge.g.keysdown[e.keyCode] > 0) {
     jsludge.g.keysdown[e.keyCode]++;
    } else {
     jsludge.g.keysdown[e.keyCode] = 1;
    }
    if (_state) _state.onkeydown(e);
  };

  _onkeyup = function (e) {
   jsludge.g.keysdown[e.keyCode] = 0;
    if (_state) _state.onkeyup(e);
  };

  _onmousedown = function (e) {
   jsludge.g.lmb = true;
    if (_state) _state.onmousedown(e);
  };

  _onmouseup = function (e) {
   jsludge.g.lmb = false;
    if (_state) _state.onmouseup(e);
  };

  _onmousemove = function (e) {
    if (_state) _state.onmousemove(e);
  };

  _addlisteners = function () {
    jsludge.util.addListener(window, 'mousemove', _onmousemove);
    jsludge.util.addListener(window, 'mousedown', _onmousedown);
    jsludge.util.addListener(window, 'mouseup', _onmouseup);
    jsludge.util.addListener(window, 'keydown', _onkeydown);
    jsludge.util.addListener(window, 'keyup', _onkeyup);
    setInterval(_tick,jsludge.g.ms);
  };

  _removelisteners = function () {
    jsludge.util.removeListener(window, 'mousemove', _onmousemove);
    jsludge.util.removeListener(window, 'mousedown', _onmousedown);
    jsludge.util.removeListener(window, 'mouseup', _onmouseup);
    jsludge.util.removeListener(window, 'keydown', _onkeydown);
    jsludge.util.removeListener(window, 'keyup', _onkeyup);
    clearInterval(_tick,jsludge.g.ms);
  };

  // ----------------------------------------------------------------------
  // global init
  jsludge.init = function (startState, renderer, gfxdata, fps) {
    jsludge.g.fps = fps || DEFAULT_FPS;
    jsludge.g.ms = 1000 / fps;
    jsludge.g.gfxdata = gfxdata;
    jsludge.g.renderer = renderer;
    renderer.loadImages(gfxdata.images);
    _state = startState;
    _addlisteners();
  };

  // ----------------------------------------------------------------------
  // global destroy
  jsludge.destroy = function () {
    _removelisteners();
    jsludge.util.destroy(this);
  };

  // ----------------------------------------------------------------------
  // global switchState
  jsludge.switchState = function (newState) {
    var oldstate = _state;
    _state = newState;
    oldstate.destroy();
  };
})(jsludge || (jsludge = {}));

