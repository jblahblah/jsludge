var jsludge;
(function (jsludge) {
  var CSSRenderer = (function(){
    function CSSRenderer(w,h,layers,bgcolor) {
      this.numLayers = layers;
      this.layers = [];
      jsludge.Renderer.call(this,w,h,bgcolor);
      this.surface = this.makeSurface(w,h,bgcolor || 'transparent');
      this.sprites = {};
      //this.elements = {};
      this.text = {};
      this.rects = {};
    }
    CSSRenderer.prototype = Object.create(jsludge.Renderer.prototype);

    CSSRenderer.prototype.makeSurface = function(w,h,bgcolor) {
      var div = document.createElement('div'), e, i;
      div.style.backgroundColor = bgcolor;
      div.style.width = w+'px';
      div.style.height = h+'px';
      div.setAttribute('id','game');
      document.body.appendChild(div);
      this.offX = div.offsetLeft;
      this.offY = div.offsetTop;
      return div;
    }

    CSSRenderer.prototype.addDiv = function(x,y) {
        var e, id;

        e = document.createElement('div');
        id = jsludge.util.getID();

        e.setAttribute('id',id);
        e.style.position = 'absolute';
        e.style.top = x+'px';
        e.style.left = y+'px';

        this.surface.appendChild(e);
        //this.elements[id] = e;

        return e;
    }

    CSSRenderer.prototype.addSprite = function(id, layer, img, src, dst) {
        var e;
        e = document.createElement('div');
        e.setAttribute('id',id);
        e.style.position = 'absolute';
        e.style.top = dst.x+'px';
        e.style.left = dst.y+'px';
        e.style.width = dst.w+'px';
        e.style.height = dst.h+'px';
        e.style.zIndex = layer;
        // get the src of the img
        e.style.background = 'url('+img.src+')';
        this.surface.appendChild(e);
        this.sprites[id] = e;
    }

    CSSRenderer.prototype.drawSprite = function(id, layer, img, src, dst) {
      if (!this.sprites[id]) {
        this.addSprite(id, layer, img, src, dst);
      }
    }

    CSSRenderer.prototype.drawMap = function() {
    }

    CSSRenderer.prototype.clear = function() {
      //this.elements = {};
      this.sprites = {};
      this.text = {};
      this.rects = {};
      this.surface.innerHTML = "";
    }

    CSSRenderer.prototype.fillRect = function(color,x,y,w,h) {
      if (!this.rects.hasOwnProperty(x+' '+y+' '+w+' '+h+' '+color)) {
        var e = this.addDiv(x,y);
        e.style.backgroundColor = color;
        e.style.width = w+'px';
        e.style.height = h+'px';
        this.rects[x+' '+y+' '+w+' '+h+' '+color] = e;
      }
    };

    CSSRenderer.prototype.drawText = function (x, y, text, color, size, font) {
      if (!this.text.hasOwnProperty(x+' '+y+text)) {
        var e = this.addDiv(x,y);
        if (!size) {
          size = '12pt';
        } else if (!isNaN(Number(size))) {
          // Number(size) will be not NaN if it's a number without a unit
          // added (e.g., 24 instead of "24px")
          size += 'pt';
        }
        e.style.color = color;
        e.style.fontFamily = font || 'sans-serif';
        e.style.fontSize = size;
        e.innerHTML = text;
        this.text[x+' '+y+text] = e;
      }
    };

    return CSSRenderer;
  }());
  jsludge.CSSRenderer = CSSRenderer;
})(jsludge || (jsludge = {}));

